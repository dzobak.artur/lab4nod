const Joi = require('joi');

const GradeCreateSchema = Joi.object({
    lastName: Joi.string()
        .min(2)
        .max(60)
        .required()
        .regex(/^[a-zA-Zа-яА-Я]+$/)
        .message('Прізвище повинно містити тільки літери'),

    group: Joi.string()
        .min(1)
        .max(60)
        .required(),

    subject: Joi.string()
        .required(),

    ticketNumber: Joi.number()
        .required()
        .integer()
        .positive()
        .message('Номер квитка має бути цілим числом і додатнім.'),

    grade: Joi.number()
        .integer()
        .required()
        .min(0)
        .max(100),

    teacher: Joi.string()
        .required()
        .regex(/^[a-zA-Zа-яА-Я]+$/)
        .message('Ім\'я вчителя повинно містити тільки літери'),
    
});

const GradeUpdateSchema = Joi.object({
    lastName: Joi.string()
        .min(2)
        .max(60)
        .regex(/^[a-zA-Zа-яА-Я]+$/)
        .message('Прізвище повинно містити тільки літери'),

    group: Joi.string()
        .min(1)
        .max(60),

    subject: Joi.string()
        .required(),

    ticketNumber: Joi.number()
        .integer()
        .positive()
        .message('Номер квитка має бути цілим числом і додатнім.'),

    grade: Joi.number()
        .integer()
        .min(0)
        .max(100)
        .required(),

    teacher: Joi.string()
        .required()
        .regex(/^[a-zA-Zа-яА-Я]+$/)
        .message('Ім\'я вчителя повинно містити тільки літери'),
    
});

module.exports = {
    GradeCreateSchema,
    GradeUpdateSchema,
};
