const express = require('express');
const router = express.Router();

const controller = require('../controllers/grades.controller'); 
const middleware = require('../middlewares/grades.middleware');

router.route('/')
    .get(controller.getGrades)
    .post(middleware.gradeCreationDataValidation,controller.createGrade);

router.route('/:gradeId')
    .get(controller.getGrade)
    .patch(middleware.gradeCreationDataValidation, controller.updateGrade)
    .delete(controller.deleteGrade);

module.exports = router;


